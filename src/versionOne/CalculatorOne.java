package versionOne;

import gui.CalculatorGui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CalculatorOne {
    public static void main(String[] args) {
        CalculatorGui calculatorGui = new CalculatorGui();
        calculatorGui.showMenu();
        int positionOfMenu = calculatorGui.getPositionOfMenu();
        calcValues(positionOfMenu);
    }

    private static void calcValues(int positionOfMenu) {
        List<Integer> values = new ArrayList<>();
        switch (positionOfMenu) {
            case 1:
                System.out.println("======Wybrano dodawanie======");
                values = getValues();
                System.out.println("Wynik dodawania to: " + (values.get(0) + values.get(1)));
                break;
            case 2:
                System.out.println("======Wybrano odejmowanie=====");
                values = getValues();
                System.out.println("Wynik odejmowania to: " + (values.get(0) - values.get(1)));
                break;
            case 3:
                System.out.println("======Wybrano mnożenie=======");
                values = getValues();
                System.out.println("Wynik mnożenia to: " + (values.get(0) * values.get(1)));
                break;
            case 4:
                System.out.println("======Wybrano dzielenie======");
                values = getValues();
                if (values.get(1) == 0) {
                    System.out.println("Dzielnik nie może być 0!");
                } else {
                    System.out.println("Wynik dzielenia to: " + (values.get(0) / values.get(1)));
                }
                break;
            default:
                System.out.println("Błąd");
        }
    }

    private static List<Integer> getValues() {
        List<Integer> values = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.print("Podaj pierwszą liczbę: ");
            int valueOne = scanner.nextInt();
            System.out.print("Podaj drugą liczbę: ");
            int valueTwo = scanner.nextInt();
            values.add(valueOne);
            values.add(valueTwo);
        } catch (Exception e) {
            System.out.println("Podałeś błędną wartość!");
            scanner.nextLine();
            getValues();
        }
        return values;
    }
}
