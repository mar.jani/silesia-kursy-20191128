package gui;

import java.util.Scanner;

public class CalculatorGui {
    final static int MIN_VALUE = 1;
    final static int MAX_VALUE = 4;

    public void showMenu() {
        System.out.println("========Kalulator============");
        System.out.println("1. Dodawani");
        System.out.println("2. Odejmowanie");
        System.out.println("3. Mnożenie");
        System.out.println("4. Dzielenie");
        System.out.println("=============================");
    }

    public int getPositionOfMenu() {
        boolean validation = false;
        int positionOfMenu = 0;
        Scanner scanner = new Scanner(System.in);

        while (!validation) {
            try {
                positionOfMenu = scanner.nextInt();
                if (validateGetPositionOfMenu(positionOfMenu)) {
                    validation = true;
                }
            } catch (Exception e) {
                System.out.println("Podano nieprawidłową wartość");
                scanner.nextLine();
            }
        }
        return positionOfMenu;
    }

    private boolean validateGetPositionOfMenu(int positionOfMenu) {
        return !(positionOfMenu < MIN_VALUE && positionOfMenu > MAX_VALUE);
    }
}
