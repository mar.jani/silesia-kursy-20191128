package versionTwo;

import gui.CalculatorGui;

import java.util.Scanner;

public class CalculatorTwo {
    public static void main(String[] args) throws Exception {
        CalculatorGui calculatorGui = new CalculatorGui();
        calculatorGui.showMenu();
        int positionOfMenu = calculatorGui.getPositionOfMenu();
        showResult(positionOfMenu);
    }

    private static void showResult(int positionOfMenu) throws Exception {
        if (positionOfMenu == 1) {
            System.out.println("======Wybrano dodawanie======");
            System.out.println("Wynik dodawania to: " + calcValue('+'));
        } else if (positionOfMenu == 2) {
            System.out.println("======Wybrano odejmowanie====");
            System.out.println("Wynik odejmowania to: " + calcValue('-'));
        } else if (positionOfMenu == 3) {
            System.out.println("======Wybrano mnożenie=======");
            System.out.println("Wynik mnożenia to: " + calcValue('*'));
        } else {
            System.out.println("======Wybrano dzielenie======");
            System.out.println("Wynik dzielenia to: " + calcValue('/'));
        }
    }

    private static int calcValue(char operand) throws Exception {
        System.out.println("Podaj pierwszą liczbę:");
        int valueOne = getValue();

        System.out.println("Podaj drugą liczbę:");
        int valueTwo = getValue();

        if (operand == '+') {
            return valueOne + valueTwo;
        } else if (operand == '-') {
            return valueOne - valueTwo;
        } else if (operand == '*') {
            return valueOne * valueTwo;
        } else {
            if(valueTwo==0){
                throw new Exception("Dzielnik nie może wynosić 0");
            }
            return valueOne / valueTwo;
        }
    }

    private static int getValue() throws Exception {
        Scanner scanner = new Scanner(System.in);
        int value = 0;
        try {
            value = scanner.nextInt();
        } catch (Exception e) {
            throw new Exception ("Błędna wartość"); //or java.lang.Error
        }
        return value;
    }
}
